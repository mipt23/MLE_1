from train import Dataset
import torch
import torch.nn.functional as F
import configparser
import os
import pandas as pd
import json

def test_pred():
    predictor = Predictor()
    predictor.predict()

class Predictor:
    def __init__(self) -> None:
        self.config = configparser.ConfigParser()
        self.config_path = os.path.join('../config.ini')
        self.config.read(self.config_path) 

        self.test_path =  self.config['DATA']['test_data']
        
        if os.path.textsplit(self.test_path)[1] == '.csv':
            self.test_data = pd.read_csv(self.test_path)
        else:
            with open(self.test_path, 'r') as json_file:
                json_test = json_file.readline()
            json_test = json.loads(json_test) 
            self.test_data = pd.DataFrame.from_dict(json_test)

        self.model_path = self.config['PARAMS']['model_path']
        self.model = torch.load(self.model_path)

    def predict(self):

        test = Dataset(self.test_data)
        test_dataloader = torch.utils.data.DataLoader(test, batch_size=2)

        use_cuda = torch.cuda.is_available()
        device = torch.device("cuda" if use_cuda else "cpu")

        predicted_label = []
        actual_label = []
        with torch.no_grad():
            for test_input, test_label in (test_dataloader):
                labels = test_label.to(device)
                attention_mask = test_input['attention_mask'].to(device)
                input_ids = test_input['input_ids'].squeeze(1).to(device)

                logits = self.model(input_ids, attention_mask)
                
                probs = F.softmax(logits, dim=1)
                output = torch.argmax(probs, dim=1)
                
                predicted_label += output
                actual_label += labels
                
        return predicted_label, actual_label
