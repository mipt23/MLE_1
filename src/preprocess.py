import pandas as pd
import re
from tqdm.auto import tqdm

import numpy as np


def clean_text(text):
  text = text.lower()
  
  # Remove account tags
  re_account_tags = r"(@[^\ ]+)"

  # Remove hastags
  re_hashtags = r"(#[^\ ]+)"

  # Remove links
  re_links = r"[^\ ]+\.([^\ ])+|([^\ ]+\:\/\/[^\ ]+)"

  # Remove extra characters (all except letters, spaces and apostrophe)
  re_extra_chars = r"[^a-z\'\ ]+"

  # Remove extra spaces
  re_extra_spaces = r"\s{2,}"

  regex = [re_account_tags, re_hashtags, re_links, re_extra_chars, re_extra_spaces]

  for _regex in regex:
    text = re.sub(_regex, ' ', text)

  # Remove end spaces 
  re_end_spaces = r"\A\s|\s$"
  text = re.sub(re_end_spaces, '', text)
  
  return text

if __name__ == "__main__":
    df = pd.read_csv('training.1600000.processed.noemoticon.csv', encoding ='latin1', header =None, 
                    names = ['target', 'id', 'date', 'flag', 'user', 'text'])
    
    df['target'] = df.target.apply(lambda target: 'negative' if target == 0 else 'positive')
    df = df[['target', 'text']]

    tqdm.pandas(desc = 'Cleaning texts..')
    df['text'] = df.text.progress_apply(lambda text: clean_text(text))
    
    tqdm.pandas(desc = 'Counting words..')
    df['words_count'] = df.text.progress_apply(lambda text: len(text.split()))

    MIN_WORDS_AMOUNT = 3
    df = df[df.words_count >= MIN_WORDS_AMOUNT]

    df_train, df_val, df_test = np.split(df.sample(frac=1, random_state=42), 
                                     [int(.8*len(df)), int(.9*len(df))])
    
    df_train[['target', 'text']].to_csv('/data/train.csv', index = False)
    df_test[['target', 'text']].to_csv('/data/test.csv', index = False)
    df_val[['target', 'text']].to_csv('/data/val.csv', index = False) 