kaggle
pandas
regex
pytest
configparser
tqdm
torch
numpy
transformers
scikit-learn
flask==2.2.2
logger==1.4
requests==2.26.0

